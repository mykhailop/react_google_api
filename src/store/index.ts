import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { sagas } from '../sagas';
import { reducer } from '../reducers';

const sagaMidddleware = createSagaMiddleware();

const store = configureStore({
  reducer,
  middleware: [sagaMidddleware],
});

sagaMidddleware.run(sagas);

export { store };
