import React from 'react';
import styles from './Footer.module.css';

export const Footer = () => {
  return (
    <div className={styles.footerContainer}>
      <span>Copyright 2020 @ Mykhailo Pylypchenko</span>
    </div>
  );
};
