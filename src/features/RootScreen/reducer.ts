import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getDistance, convertDistance } from 'geolib';
import { availableUnits } from '../../constants';
// isValidCoordinate,

const initialState = {
  distance: 0,
  isLoading: false,
  firstAddress: {
    isValid: true,
    value: undefined,
  },
  secondAddress: {
    isValid: true,
    value: undefined,
  },
  unit: {
    isValid: true,
    value: 'm',
  },
};

export const { actions, name, reducer } = createSlice({
  name: 'root',
  initialState,
  reducers: {
    setAndValidateUnit(state, action: PayloadAction<any>) {
      // simple validation for unit, we could create validation module in future
      if (availableUnits.includes(action.payload.unit)) {
        state.unit.isValid = true;
        state.unit.value = action.payload.unit;
      } else {
        state.unit.isValid = false;
      }
    },
    setAndValidateFirstAddress(state, action: PayloadAction<any>) {
      state.firstAddress.value = action.payload.firstAddress;
      // TODO add validation here
    },
    setAndValidateSecondAddress(state, action: PayloadAction<any>) {
      state.secondAddress.value = action.payload.secondAddress;
      // TODO add validation here
    },
    calculate(state) {
      state.isLoading = true;
    },
    calculateGoogleResponse(state) {
      state.isLoading = false;

      state.distance = convertDistance(
        getDistance(
          { latitude: 50.094667, longitude: 19.987657 },
          { latitude: 50.072503, longitude: 19.968435 },
        ),
        state.unit.value,
      );
    },
  },
});

export const {
  calculate,
  calculateGoogleResponse,
  setAndValidateUnit,
  setAndValidateFirstAddress,
  setAndValidateSecondAddress,
} = actions;

export const selectDistance = (state: any) => state[name].distance;
export const selectFirstAndSecondValue = (state: any) => ({
  firstAddress: state[name].firstAddress.value,
  secondAddress: state[name].secondAddress.value,
});
export const selectIsLoading = (state: any) => state[name].isLoading;
export const selectUnit = (state: any) => state[name].unit.value;
export const selectIsValid = (state: any) => ({
  firstAddress: state[name].firstAddress.isValid,
  secondAddress: state[name].secondAddress.isValid,
  unit: state[name].unit.isValid,
});
