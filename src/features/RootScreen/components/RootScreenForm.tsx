import React, { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { values } from 'lodash';
import { Button } from '../../../components/Button';
import { Input } from '../../../components/Input';
import { Validation } from '../../../components/Validation';

import {
  calculate,
  selectIsLoading,
  selectIsValid,
  setAndValidateUnit,
  setAndValidateFirstAddress,
  setAndValidateSecondAddress,
} from '../reducer';

import styles from '../RootScreen.module.css';

export const RootScreenForm = () => {
  const [firstAddress, setFirstAddress] = useState('');
  const [secondAddress, setSecondAddress] = useState('');
  const [unit, setUnit] = useState('m');

  const isLoading = useSelector(selectIsLoading);
  const isValid = useSelector(selectIsValid);

  const firstValueChange = useCallback((e: any) => setFirstAddress(e.target.value), [
    setFirstAddress,
  ]);

  const secondValueChange = useCallback((e: any) => setSecondAddress(e.target.value), [
    setSecondAddress,
  ]);

  const setUnitCallback = useCallback((e: any) => setUnit(e.target.value), [setUnit]);

  const dispatch = useDispatch();

  return (
    <>
      <div className={styles.inputsContainer}>
        <Validation isValid={isValid.firstAddress}>
          <Input
            onBlur={() => dispatch(setAndValidateFirstAddress({ firstAddress }))}
            placeholder="Enter first coordinate"
            onChange={firstValueChange}
          />
        </Validation>

        <Validation isValid={isValid.secondAddress}>
          <Input
            onBlur={() => dispatch(setAndValidateSecondAddress({ secondAddress }))}
            placeholder="Enter second coordinate"
            onChange={secondValueChange}
          />
        </Validation>

        <Validation isValid={isValid.unit}>
          <Input
            value={unit}
            placeholder="Enter distance unit (m or km)"
            onBlur={() => dispatch(setAndValidateUnit({ unit }))}
            onChange={setUnitCallback}
          />
        </Validation>
      </div>

      <div className={styles.buttonsContainer}>
        <Button
          onClick={() => dispatch(calculate())}
          disabled={
            !firstAddress || !secondAddress || !unit || isLoading || values(isValid).includes(false)
          }
        >
          Calculate
        </Button>
      </div>
    </>
  );
};
