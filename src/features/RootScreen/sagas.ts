import { all, put, call, takeLeading, select } from 'redux-saga/effects';
import { calculate, calculateGoogleResponse, selectFirstAndSecondValue } from './reducer';

const delay = (time: number) => new Promise((resolve) => setTimeout(resolve, time));

export function* calculateSideEffect() {
  // TODO add here request to GoogleAPI
  try {
    // const payload = yield call(API, params)
    // payload processing
    const data = yield select(selectFirstAndSecondValue);
    console.warn('data is loading', data);
  } finally {
    yield call(delay, 5000);
    yield put(calculateGoogleResponse());
  }
}

export function* sagas() {
  yield all([takeLeading(calculate, calculateSideEffect)]);
}
