import React from 'react';
import { useSelector } from 'react-redux';
import { Loader } from '../../components/Loader';
import { MapContainer } from '../../components/Map';
import { API_KEY } from '../../constants';

import { RootScreenForm } from './components';

import { selectDistance, selectIsLoading, selectUnit } from './reducer';

import styles from './RootScreen.module.css';

const additionalProps = {
  googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${API_KEY}&v=3.exp&libraries=geometry,drawing,places`,
  loadingElement: <div style={{ height: `100%` }} />,
  containerElement: <div style={{ height: `100%` }} />,
  mapElement: <div style={{ height: `100%` }} />,
};

export const RootScreen = () => {
  const distance = useSelector(selectDistance);
  const isLoading = useSelector(selectIsLoading);
  const unitFromState = useSelector(selectUnit);

  return (
    <div className={styles.rootScreenContainer}>
      <div className={styles.leftColumn}>
        <div className={styles.distanceContainer}>
          <span>Distance:</span>
          {isLoading ? <Loader /> : `${distance} ${unitFromState}`}
        </div>
        <RootScreenForm />
      </div>

      <div className={styles.mapsContainer}>{null && <MapContainer {...additionalProps} />}</div>
    </div>
  );
};
