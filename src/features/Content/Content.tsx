import React from 'react';
import { RootScreen } from '../RootScreen';

import styles from './Content.module.css';

export const Content = () => {
  return (
    <div className={styles.contentContainer}>
      <RootScreen />
    </div>
  );
};
