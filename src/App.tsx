import React from 'react';
// import { Footer } from './features/Footer';
import { Content } from './features/Content';

import styles from './App.module.css';

function App() {
  return (
    <div className={styles.appContainer}>
      <Content />
    </div>
  );
}

export default App;
