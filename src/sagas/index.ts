import { all, spawn } from 'redux-saga/effects';
import { sagas as rootScreenSagas } from '../features/RootScreen';

export function* sagas() {
  yield all([spawn(rootScreenSagas)]);
}
