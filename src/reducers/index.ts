import { combineReducers } from '@reduxjs/toolkit';
import { name as root, reducer as rootReducer } from '../features/RootScreen';

export const reducer = combineReducers({
  [root]: rootReducer,
});
