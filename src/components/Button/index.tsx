import React from 'react';

import styles from './index.module.css';

interface ButtonProps {
  disabled?: boolean;
  onClick: () => void;
}

export const Button: React.FC<ButtonProps> = ({ children, onClick, disabled }) => {
  return (
    <button className={styles.button} onClick={onClick} disabled={disabled}>
      {children}
    </button>
  );
};
