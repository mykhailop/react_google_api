import React from 'react';
import { Button } from '../index';
import renderer from 'react-test-renderer';

test('Test Button component', () => {
  let tree = renderer.create(<Button onClick={() => {}} />).toJSON();

  expect(tree).toMatchSnapshot();

  tree = renderer
    .create(
      <Button
        onClick={() => {
          console.warn('Button is clicked');
        }}
      />,
    )
    .toJSON();

  expect(tree).toMatchSnapshot();

  tree = renderer
    .create(
      <Button
        onClick={() => {
          console.warn('Button is clicked');
        }}
        disabled
      />,
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
