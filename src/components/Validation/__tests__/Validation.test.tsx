import React from 'react';
import { Validation } from '../index';
import renderer from 'react-test-renderer';

test('Test Validation component', () => {
  let tree = renderer.create(<Validation isValid={true} />).toJSON();

  expect(tree).toMatchSnapshot();

  tree = renderer.create(<Validation isValid={true} validationMsg="Enter a valid data" />).toJSON();

  expect(tree).toMatchSnapshot();

  tree = renderer
    .create(<Validation isValid={false} validationMsg="Enter a valid data" />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
