import React from 'react';
import styles from './index.module.css';

interface ValidationProps {
  isValid: boolean;
  validationMsg?: string;
}

export const Validation: React.FC<ValidationProps> = ({
  isValid,
  validationMsg = 'Enter a correct value',
  children,
}) => (
  <div className={styles.iconWrapper}>
    {children}
    {!isValid ? <div className={styles.message}>{validationMsg}</div> : null}
  </div>
);
