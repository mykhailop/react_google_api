import React from 'react';
import { Input } from '../index';
import renderer from 'react-test-renderer';

test('Test Input component', () => {
  let tree = renderer.create(<Input onChange={() => {}} />).toJSON();

  expect(tree).toMatchSnapshot();

  tree = renderer
    .create(
      <Input
        placeholder="Enter your value"
        onChange={(e) => {
          console.warn(e.target.value);
        }}
      />,
    )
    .toJSON();

  expect(tree).toMatchSnapshot();

  tree = renderer
    .create(
      <Input
        value="Backer Streer, London"
        placeholder="Enter your value"
        onChange={(e) => {
          console.warn(e.target.value);
        }}
      />,
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
