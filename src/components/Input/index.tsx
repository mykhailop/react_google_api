import React from 'react';

import styles from './index.module.css';

interface InputProps {
  value?: string;
  placeholder?: string;
  onChange: (e: any) => void;
  onBlur?: () => void;
}

export const Input: React.FC<InputProps> = ({ onChange, placeholder, value, onBlur }) => {
  return (
    <input
      placeholder={placeholder}
      className={styles.input}
      onChange={onChange}
      value={value}
      onBlur={onBlur}
    />
  );
};
