import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

import styles from './index.module.css';

interface MapProps {
  isMarkerShown?: boolean;
}

export const MapContainer = withScriptjs(
  withGoogleMap(({ isMarkerShown }: MapProps) => {
    return (
      <div className={styles.map}>
        <GoogleMap defaultZoom={8} defaultCenter={{ lat: -34.397, lng: 150.644 }}>
          {isMarkerShown && <Marker position={{ lat: -34.397, lng: 150.644 }} />}
        </GoogleMap>
      </div>
    );
  }),
);
